package com.wlp.commonapi.echo;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
public interface EchoService {
    String echo(String message);
}
