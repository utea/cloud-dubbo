package com.wlp.provider.rocketmq.customer;

import org.apache.rocketmq.client.consumer.MQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
public abstract class AbstractRocketConsumer implements RocketConsumer{
    public String topics;
    public String tags;
    public MessageListenerConcurrently messageListener;
    public String consumerTitel;
    public MQPushConsumer mqPushConsumer;

    /**
     * 必要的信息
     *
     * @param topics
     * @param tags
     * @param consumerTitel
     */
    public void necessary(String topics, String tags, String consumerTitel) {
        this.topics = topics;
        this.tags = tags;
        this.consumerTitel = consumerTitel;
    }

    @Override
    public abstract void init();

    @Override
    public void registerMessageListener(MessageListenerConcurrently messageListener) {
        this.messageListener = messageListener;
    }
}
