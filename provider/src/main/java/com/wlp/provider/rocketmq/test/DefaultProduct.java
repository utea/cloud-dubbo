package com.wlp.provider.rocketmq.test;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Component
public class DefaultProduct {
    @Autowired(required = false)
    private DefaultMQProducer defaultMQProducer;

    /**
     * 发送消息
     * @param message
     * @return
     */
    public String send(String message) {
        Message msg = null;
        try {
            msg = new Message("TopicTest", "tags1", message.getBytes(RemotingHelper.DEFAULT_CHARSET));
            // 发送消息到一个Broker
            SendResult sendResult = defaultMQProducer.send(msg);
            // 通过sendResult返回消息是否成功送达
            System.out.printf("%s%n", sendResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
