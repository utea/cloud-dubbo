package com.wlp.provider.rocketmq.customer;

import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
public interface RocketConsumer {
    /**
     * 初始化消费者
     */
    void init();

    /**
     * 注册监听
     *
     * @param messageListener
     */
    void registerMessageListener(MessageListenerConcurrently messageListener);
}
