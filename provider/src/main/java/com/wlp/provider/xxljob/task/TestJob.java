package com.wlp.provider.xxljob.task;

import com.wlp.provider.rocketmq.test.DefaultProduct;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Component
public class TestJob {

    @Autowired
    private DefaultProduct product;

    @XxlJob("demoJobHandler2")
    public ReturnT<String> demoJobHandler2(String param) {
        product.send("XXL-JOB2:"+System.currentTimeMillis());
        return ReturnT.SUCCESS;
    }
}
