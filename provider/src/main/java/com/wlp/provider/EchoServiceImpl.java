package com.wlp.provider;

import com.wlp.commonapi.echo.EchoService;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@DubboService
public class EchoServiceImpl implements EchoService {

    @Override
    public String echo(String message) {
        return "[echo] Hello, " + message;
    }
}
