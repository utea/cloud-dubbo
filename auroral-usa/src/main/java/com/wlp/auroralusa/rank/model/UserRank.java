package com.wlp.auroralusa.rank.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Data
@Accessors(chain = true)
public class UserRank {
    @TableId
    private Integer id;
    private Double score;
    private Date createTime;
    private Date updateTime;
}
