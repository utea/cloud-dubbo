package com.wlp.auroralusa.rank.dao.master;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlp.auroralusa.rank.model.UserRank;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MasterUserRankDao extends BaseMapper<UserRank> {
    @Select("select * from user_rank order by score desc limit 10")
    List<UserRank> top10();

    int batchUpd(List<UserRank> list);
}
