package com.wlp.auroralusa.rank.dao.master;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlp.auroralusa.rank.model.DelayRecord;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Mapper
public interface DelayRecordDao {
    @Insert("insert into delay_record(delay,area) values(#{delay},#{area})")
    void insert(DelayRecord delayRecord);
}
