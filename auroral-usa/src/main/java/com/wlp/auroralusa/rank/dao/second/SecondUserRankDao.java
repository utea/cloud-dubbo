package com.wlp.auroralusa.rank.dao.second;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlp.auroralusa.rank.model.UserRank;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Mapper
public interface SecondUserRankDao extends BaseMapper<UserRank> {
    @Select("select * from user_rank order by score desc limit 10")
    List<UserRank> top10();
}
