package com.wlp.auroralusa.rank.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Data
@Accessors(chain = true)
public class DelayRecord {
    @TableId
    private Integer id;
    private Integer delay;
    private String area;
}
