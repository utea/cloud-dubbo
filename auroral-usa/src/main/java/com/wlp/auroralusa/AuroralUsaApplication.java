package com.wlp.auroralusa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AuroralUsaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuroralUsaApplication.class, args);
    }

}
