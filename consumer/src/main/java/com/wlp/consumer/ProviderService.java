package com.wlp.consumer;

import cn.hutool.core.util.RandomUtil;
import com.wlp.commonapi.echo.EchoService;
import com.wlp.consumer.common.RedisUtil;
import com.wlp.consumer.model.People;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Service
public class ProviderService {
    @DubboReference
    private EchoService echoService;
    @Autowired
    private RedisUtil redisUtil;

    public String echo(String name){
        return echoService.echo(name);
    }

    public void testRedis(){
//        final String key = "rank";
//        int i = RandomUtil.randomInt(1,10);
//        People p = new People().setId(i).setName("test"+i);
//        Double d = RandomUtil.randomDouble(10,100);
//        redisUtil.zSet(key,p,d);
    }
}
