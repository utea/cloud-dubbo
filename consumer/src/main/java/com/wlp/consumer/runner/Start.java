package com.wlp.consumer.runner;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.wlp.consumer.common.RedisUtil;
import com.wlp.consumer.model.People;
import com.wlp.consumer.task.RankTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.math.RoundingMode;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Component
public class Start implements CommandLineRunner {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RankTask rankTask;

    private final String KEY = "rank";
    @Override
    public void run(String... args) throws Exception {
        Long nt = System.currentTimeMillis();
        System.out.println(nt+"调度更新");
        Mono.fromRunnable(()->{
            while (true){
                rankTask.rank();
            }
        }).subscribeOn(Schedulers.parallel()).subscribe();
        System.out.println(nt+"调度更新完成");
    }
}
