package com.wlp.consumer.model;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Data
@Accessors(chain = true)
public class People {
    private int id;
    private Double score;
}
