package com.wlp.consumer.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Data
@Accessors(chain = true)
public class RedisDelay {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String area;
    private Integer delay;
    private Date createTime;
}
