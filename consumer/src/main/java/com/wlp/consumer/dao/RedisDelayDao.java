package com.wlp.consumer.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlp.consumer.model.RedisDelay;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Mapper
public interface RedisDelayDao extends BaseMapper<RedisDelay> {
}
