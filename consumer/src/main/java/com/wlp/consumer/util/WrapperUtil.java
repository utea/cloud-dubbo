package com.wlp.consumer.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
public class WrapperUtil {
    private WrapperUtil(){}

    public static <T> QueryWrapper<T> query(T obj){
        if(obj==null){
            return null;
        }
        QueryWrapper<T> qw = new QueryWrapper<>();
        Arrays.stream(ReflectUtil.getFields(obj.getClass()))
                .forEach(f-> Optional.ofNullable(ReflectUtil.getFieldValue(obj,f)).filter(ObjectUtil::isNotEmpty)
                        .ifPresent(v-> qw.eq(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.getName()),v)));
        return qw;
    }
}
