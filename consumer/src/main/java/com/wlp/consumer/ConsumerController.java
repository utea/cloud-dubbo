package com.wlp.consumer;

import com.alibaba.nacos.client.naming.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@RestController
public class ConsumerController {
    @Autowired
    ProviderService providerService;

    @GetMapping("/echo/{name}")
    public String echo(@PathVariable("name")String name){
        new Thread(()->{
            System.out.println(">>>>>>>>>>>>>> 线程执行开始");
            getD(10);
            System.out.println(">>>>>>>>>>>>>> 线程执行结束");
        }).start();
        return providerService.echo(name);
    }

    @GetMapping("/test")
    public void test(){
        providerService.testRedis();
    }

    @GetMapping("/quit")
    public void quit(){
        System.exit(0);
    }

    private boolean getD(int p){
        long nt = System.currentTimeMillis()/1000;
        long nt1 = System.currentTimeMillis()/1000,nt2;
        int i=0;
        while (nt1-nt < p){
            nt2 = nt1;
            nt1 = System.currentTimeMillis()/1000;
            if(nt1-nt2==1){
                i++;
            }
        }
        return true;
    }
}
