package com.wlp.consumer.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 美西 redis
 *
 * @Author: wanglp
 */
@Configuration
public class RedisOregonConfig {

    @Value("${spring.redis-read-only-two.host}")
    private String host;
    @Value("${spring.redis-read-only-two.port}")
    private String port;
    @Value("${spring.redis-read-only-two.database:0}")
    private String database;
    @Value("${spring.redis-read-only-two.password:}")
    private String password;
    @Value("${spring.redis-read-only-two.lettuce.pool.max-active}")
    private String maxActive;
    @Value("${spring.redis-read-only-two.lettuce.pool.max-idle}")
    private String maxIdle;
    @Value("${spring.redis-read-only-two.lettuce.pool.max-wait}")
    private String maxWait;
    @Value("${spring.redis-read-only-two.lettuce.pool.min-idle}")
    private String minIdle;
    @Value("${spring.redis-read-only-two.lettuce.timeout}")
    private String timeout;


    /**
     * 单实例redis数据源
     *
     * @return
     */
    @Bean
    public RedisTemplate<String, Object> redisReadOnlyOregonTemplate() {

        /* ========= 基本配置 ========= */
        RedisStandaloneConfiguration configuration = RedisConfig.redisReadOnlyConfig(host,port,database,password);

        /* ========= 连接池通用配置 ========= */
        GenericObjectPoolConfig genericObjectPoolConfig = RedisConfig.redisReadOnlyPool(minIdle,maxIdle,maxActive,maxWait);

        return RedisConfig.buildTemplate(RedisConfig.buildFactory(genericObjectPoolConfig,configuration,timeout));
    }

}