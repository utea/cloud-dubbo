package com.wlp.auroralsig.rank.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlp.auroralsig.rank.model.UserRank;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Mapper
public interface UserRankDao extends BaseMapper<UserRank> {
}
