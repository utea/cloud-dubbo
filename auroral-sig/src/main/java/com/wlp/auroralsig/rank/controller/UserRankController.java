package com.wlp.auroralsig.rank.controller;

import com.wlp.auroralsig.rank.service.UserRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@RestController
@RequestMapping("/user_rank")
public class UserRankController {
    @Autowired
    private UserRankService userRankService;

    @GetMapping("/test")
    public String test(){
        userRankService.updRank();
        return "success";
    }
}
