package com.wlp.auroralsig.rank.service;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wlp.auroralsig.rank.dao.UserRankDao;
import com.wlp.auroralsig.rank.model.UserRank;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: wanglp
 */
@Service
public class UserRankService extends ServiceImpl<UserRankDao, UserRank> {
    /**
     * 更新rank数据
     */
    public void updRank(){
        Integer[] ids = {1,2,3,4,5,6,7,8,9,10};
       //Integer[] ids = {11,12,13,14,15,16,17,18,19,20};
//        Integer[] ids = {21,22,23,24,25,26,27,28,29,30};
        List<UserRank> list = Arrays.stream(ids)
                .map(id-> new UserRank().setId(id).setScore(RandomUtil.randomDouble(50,100)).setCreateTime(new Date()).setUpdateTime(new Date()))
                .collect(Collectors.toList());
        super.updateBatchById(list);
    }
}
