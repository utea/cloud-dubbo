package com.wlp.auroralsig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AuroralSigApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuroralSigApplication.class, args);
    }

}
